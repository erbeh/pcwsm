# Plant Cell Wall Saccharification Model (PCWSM)


Within this repository, you can find the code of our model for the saccharification of a lignocellulosic plant cell wall sub-unit, composed of a cellulose microfibril surrounded by hemicellulose and lignin.

## Content

The repository is split into three folders, each of which are summarized below.

### Folder "Simulation_code"

This folder contains the newest version of our model's code, written in C++. It also contains scripts for averaging the output data, written in Python (version 3.7). A further README for running all of these is provided within the folder

### Folder "Optimization_algorithm"

This folder contains our algorithm for fitting the kinetic parameters of the model, as well as a README for running it. 

### Folder "Figure_data"

This folder contains the data, plotting scripts and figures which we use in the paper connected to this repository.