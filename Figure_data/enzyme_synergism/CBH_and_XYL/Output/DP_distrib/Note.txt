The raw data for these figures takes too much disk space to include it in a git repository. You can either re-do the simulations locally, or contact us, if you are interested in the original sets.
